class CreateStocks < ActiveRecord::Migration[5.2]
  def change
    create_table :stocks do |t|
      t.integer :id_product
      t.integer :unidade
      t.integer :quantidade_vendas

      t.timestamps
    end
  end
end
