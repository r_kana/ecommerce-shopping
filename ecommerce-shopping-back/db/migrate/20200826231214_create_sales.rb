class CreateSales < ActiveRecord::Migration[5.2]
  def change
    create_table :sales do |t|
      t.date :data
      t.float :total
      t.string :produtos

      t.timestamps
    end
  end
end
