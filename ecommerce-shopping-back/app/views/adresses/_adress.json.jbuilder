json.extract! adress, :id, :cep, :estado, :bairro, :cidade, :nome, :rua, :complemento, :numero, :tipo, :created_at, :updated_at
json.url adress_url(adress, format: :json)
