json.extract! product, :id, :nome, :imagem, :descricao, :categoria, :preco, :likes, :peso, :dimensao, :created_at, :updated_at
json.url product_url(product, format: :json)
