json.extract! user, :id, :email, :senha, :tipo, :nome, :sobrenome, :telefone_fixo, :telefone_movel, :created_at, :updated_at
json.url user_url(user, format: :json)
