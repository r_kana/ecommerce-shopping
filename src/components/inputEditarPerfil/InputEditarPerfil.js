import React from 'react';
import './InputEditarPerfil.css';

function InputEditarPerfil() {
  return (
    <div className="input-editar-perfil">
      <div className="titulo-bto">
        <h2>Editar Perfil</h2>
        <div>
          <button>Cancelar</button>
          <button>Confirmar</button>
        </div>
      </div>
      <div className="input-editar-perfil-usuarios">
        <label>Nome: <input /></label>
        <label>Email: <input /></label>
        <label>Senha: <input /></label>
        <label>Confirme Senha: <input /></label>
        <label>Telefone Fixo: <input /></label>
        <label>Telefone Celular: <input /></label>
      </div>
      <div className="enderecos">
        <h2>Endereços:</h2>
        <div>
          <p>Casa</p>
          <p>Trabalho</p>
          <p>Casa da mãe</p>
        </div>
      </div>
      <div className="input-editar-enderecos">
        <label>Como gostaria de salvar o endereço: <input /></label>
        <label>Nome completo de quem vai receber: <input /></label>
        <label>Endereço: <input id="endereco-input-1"/></label>
        <div id="input-editar-enderecos-1">
          <label>Numero: <input id="numero"/></label>
          <label>Bairro: <input /></label>
        </div>
        <div id="input-editar-enderecos-2">
          <label>Cidade: <input /></label>
          <label>UF: <input /></label>
        </div>
      </div>
    </div>
  )
}

export default InputEditarPerfil;
