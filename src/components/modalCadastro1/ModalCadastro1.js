import React from 'react';
import './ModalCadastro1.css';

function ModalCadastro1() {
  return (
    <div className="modal" id="modal-cadastro1">
      <div id="container-modal-cadastro">
        <a href="#fechar" title="Fechar" className="fechar">X</a>
        <h2>Fazer Cadastro</h2>
        <div className="barra-modal-cadastro"></div>
        <p>Nome</p>
        <input type="text"/>
        <p>Email</p>
        <input type="email"/>
        <p>Senha</p>
        <input type="password"/>
        <p>Confirmar Senha</p>
        <input type="password"/>
        <a href="#modal-cadastro2"><button>Continuar</button></a>
      </div>
    </div>
  )
}

export default ModalCadastro1;
