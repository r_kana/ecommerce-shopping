import React from 'react';
import './SecaoChamativa.css';

function SecaoChamativa() {
  return (
    <div className="secao-chamativa">
      <h2>Shopp{"{IN}"}g</h2>
      <hr id="hr-chamativo"/>
    </div>
  )
}

export default SecaoChamativa;
