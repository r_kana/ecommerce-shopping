import React from 'react';
import sobre from '../../assets/img/sobre.jpg';
import './SecaoSobre.css';

function SecaoSobre() {
  return (
    <div className="sobre">
      <img src={sobre} alt="imagem sobre da empresa"/>
      <div className="texto-sobre">
        <h2>Sobre o e-Commerce Shopp{"{IN}"}g</h2>
        <p>Mussum Ipsum, cacilds vidis litro abertis. Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis. Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget. Interagi no mé, cursus quis, vehicula ac nisi. Sapien in monti palavris qui num significa nadis i pareci latim.</p>
      </div>
    </div>
  )
}

export default SecaoSobre;
