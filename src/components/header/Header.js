import React from 'react';
import { Link } from 'react-router-dom';
import './Header.css';
import logo from '../../assets/img/logo.png';

function Header() {
  return (
    <div className="header">
      <div>
        <Link to="/"><img src={logo} alt="Logo da empresa."/></Link>
        <ul>
          <li><Link to="/produtos">Produtos</Link></li>
          <li><a href="#modal-login">Login</a></li>
          <li><a href="#modal-cadastro1">Cadastro</a></li>
        </ul>
      </div>
    </div>
  )
}

export default Header;
