import React from 'react';
import './Footer.css';
// icones
import iconeFacebook from '../../assets/icones/facebook.svg';
import iconeTwitter from '../../assets/icones/twitter.svg';
import iconeInstagram from '../../assets/icones/instagram.svg';

function Footer() {;
  return (
    <div className="x">
      <div className="footer">
        <div className="container-footer">
          <div className="icones-copyrights">
            <div className="icones">
              <a href="https://www.facebook.com/shopping"><img src={iconeFacebook} alt="icone do facebook" /></a>
              <a href="https://www.twitter.com/shopping"><img src={iconeTwitter} alt="icone do twitter" /></a>
              <a href="https://www.instagram.com/shopping"><img src={iconeInstagram} alt="icone do instagram" /></a>
            </div>
            <p>@Copyright 2020 e-Commerce Shopp{"{IN}"}g</p>
          </div>
          <div className="desenvolvido">
            <p>Desenvolvido por: Leonardo Freitas, Julia Freitas e Pedro Lima.</p>
          </div>
        </div>
      </div>
    </div>
  )
};

export default Footer;
