import React, { useState } from 'react';
import './ModalCadastro2.css';
import axios from 'axios';

// url da api correios
const url = "https://viacep.com.br/ws/";

function ModalCadastro1() {

  // declarando no hooks
  const [CEP, setCEP] = useState("");
  const [endereco, setEndereco] = useState("");
  const [numero, setNumero] = useState("");
  const [bairro, setBairro] = useState("");
  const [cidade, setCidade] = useState("");
  const [UF, setUF] = useState("");

  // FUNCOES
  // limpar o formulario
  const limpaFormulario = () => {
    setEndereco("");
    setNumero("");
    setBairro("");
    setCidade("");
    setUF("");
  };
  // colocar as informacoes no input
  const meuCallBack = (conteudo) => {
    if (!("erro" in conteudo)) {
      setEndereco(conteudo.logradouro);
      setBairro(conteudo.bairro);
      setCidade(conteudo.localidade);
      setUF(conteudo.uf);
    } else {
      limpaFormulario();
      alert("CEP não encontrado!");
    }
  };
  // chmando a api
  const chamandoApi = async (cep) => {
    await axios.get(`${url}/${cep}/json`)
      .then(res => {
        // console.log(res);
        meuCallBack(res.data);
      })
  };
  return (
    <div className="modal" id="modal-cadastro2">
      <div id="container-modal-cadastro">
        <a href="#fechar" title="Fechar" className="fechar">X</a>
        <h2>Fazer Cadastro</h2>
        <div className="barra-modal-cadastro">
          <div id="escura"></div>
        </div>
        <div className="modal-input-2">
          <div>
            <p>CEP</p>
            <input 
              onChange={e => setCEP(e.target.value)} 
              name="CEP" 
              type="text" 
              value={CEP} 
            />
          </div>
          <button 
            id="bto-buscar" 
            onClick={() => chamandoApi(CEP)}
          >Buscar</button>
        </div>
        <p>Endereço</p>
        <input 
          name="endereco" 
          type="text"
          value={endereco} 
          onChange={e => setEndereco(e.target.value)}
        />
        <div className="modal-input-2">
          <div>
            <p>Numero</p>
            <input 
              name="numero" 
              type="text"
              value={numero} 
              onChange={e => setNumero(e.target.value)}
            />
          </div>
          <div>
            <p>Bairro</p>
            <input 
              name="bairro" 
              type="text"
              value={bairro} 
              onChange={e => setBairro(e.target.value)}
            />
          </div>
        </div>
        <div className="modal-input-2">
          <div>
            <p>Cidade</p>
            <input 
              name="cidade" 
              type="text"
              value={cidade} 
              onChange={e => setCidade(e.target.value)}
            />
          </div>
          <div>
            <p>UF</p>
            <input 
              name="UF" 
              type="text"
              value={UF} 
              onChange={e => setUF(e.target.value)}
            />
          </div>
        </div>
        <a href="#modal-cadastro3"><button>Continuar</button></a>
      </div>
    </div>
  )
}

export default ModalCadastro1;
