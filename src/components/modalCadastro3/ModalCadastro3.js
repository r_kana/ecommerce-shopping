import React from 'react';
import './ModalCadastro3.css';

function ModalCadastro1() {
  return (
    <div className="modal" id="modal-cadastro3">
      <div id="container-modal-cadastro-3">
        <a href="#fechar" title="Fechar" className="fechar">X</a>
        <h2>Fazer Cadastro</h2>
        <div id="barra-modal-cadastro-3"></div>
        <p>Telefone Fixo</p>
        <input type="text"/>
        <p>Telefone Celular</p>
        <input type="text"/>
        <a href="#fechar"><button>Cadastrar</button></a>
      </div>
    </div>
  )
}

export default ModalCadastro1;
